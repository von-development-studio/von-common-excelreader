/**
 * @Class:      ExcelReader
 * @Created:    09-12-2022
 * @Updated:
 * @CopyRight:	VON Development Studio 2017
 */
package ec.com.von.common.excelreader

/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
data class MockExcelFile(
  var column1: String? = null,
  var column2: String? = null,
  var column3: String? = null,
  var column4: String? = null
)