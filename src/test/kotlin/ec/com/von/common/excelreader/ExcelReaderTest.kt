/**
 * @Class:      ExcelReader
 * @Created:    09-12-2022
 * @Updated:
 * @CopyRight:	VON Development Studio 2017
 */
package ec.com.von.common.excelreader

import org.apache.poi.ss.usermodel.Row
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.mock.web.MockMultipartFile
import java.io.File
import java.io.FileInputStream
import java.io.IOException


/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
@DisplayName("Excel Reader")
open class ExcelReaderTest {

  @Test
  @DisplayName("Should throw exception when excel file is not valid")
  @Throws(
    IOException::class,
    ExcelReaderException::class
  )
  open fun shouldThrowExcelReaderExceptionWhenExcelFileIsNotValid() {
    Assertions.assertThrows(ExcelReaderException::class.java) {
      val fileName = "file"
      val resource = javaClass.getResource(String.format("/%s", fileName))
      val excelFile = File(resource!!.file)
      val excelInputStream = FileInputStream(excelFile)
      val multiPartFile = MockMultipartFile(fileName, fileName, "", excelInputStream)
      ExcelReader<Any>(multiPartFile)
    }
  }

  @Test
  @DisplayName("Should throw exception when no end condition is defined")
  @Throws(
    ExcelReaderException::class
  )
  open fun shouldThrowExcelReaderExceptionWhenNoEndConditionDefined() {
    Assertions.assertThrows(ExcelReaderException::class.java) {
      val excelReader = createExcelReaderObject("file.xlsx")
      excelReader!!.read()
    }
  }

  @Test
  @DisplayName("Should throw exception when no row mapping is defined")
  @Throws(
    ExcelReaderException::class
  )
  open fun shouldThrowExcelReaderExceptionWhenNoRowMappingDefined() {
    Assertions.assertThrows(ExcelReaderException::class.java) {
      createExcelReaderObject("file.xlsx")
        ?.setEndCondition { true }
        ?.read()
    }
  }

  @Test
  @DisplayName("Should return four elements when no header height is defined")
  @Throws(
    ExcelReaderException::class
  )
  open fun shouldReturnFourElementsWhenNoHeaderHeightDefined() {
    val excelReader = createExcelReaderObject("valid.xlsx")
    val expectedSize = 4
    val result: ArrayList<MockExcelFile> = excelReader
      ?.setEndCondition { row: Row ->
        row.getCell(0).stringCellValue.isBlank()
      }
      ?.setMapping { row: Row ->
        val mockExcel = MockExcelFile()
        mockExcel.column1 = row.getCell(0).stringCellValue
        mockExcel.column2 = row.getCell(1).stringCellValue
        mockExcel.column3 = row.getCell(2).stringCellValue
        mockExcel.column4 = row.getCell(3).stringCellValue
        mockExcel
      }
      ?.read()
      ?.result!!
    Assertions.assertEquals(expectedSize, result.size)
  }

  @Test
  @DisplayName("Should return three elements when header height is defined")
  @Throws(
    ExcelReaderException::class
  )
  open fun shouldReturnThreeElementsWhenHeaderHeightIsDefined() {
    val excelReader = createExcelReaderObject("valid.xlsx")
    val expectedSize = 3
    val result = excelReader
      ?.setHeader(1)
      ?.setEndCondition { row: Row ->
        row.getCell(0).stringCellValue.isBlank()
      }
      ?.setMapping { row: Row ->
        val mockExcel = MockExcelFile()
        mockExcel.column1 = row.getCell(0).stringCellValue
        mockExcel.column2 = row.getCell(1).stringCellValue
        mockExcel.column3 = row.getCell(2).stringCellValue
        mockExcel.column4 = row.getCell(3).stringCellValue
        mockExcel
      }
      ?.read()
      ?.result!!
    Assertions.assertEquals(expectedSize, result.size)
  }

  @Test
  @DisplayName("Should return nine elements when header height is defined and read all is true")
  @Throws(
    ExcelReaderException::class
  )
  open fun shouldReturnNineElementsWhenHeaderHeightIsDefinedAndReadAllIsTrue() {
    val excelReader = createExcelReaderObject("valid.xlsx")
    val expectedSize = 9
    val result = excelReader
      ?.setHeader(1)
      ?.setEndCondition { row: Row ->
        row.getCell(0).stringCellValue.isBlank()
      }
      ?.setMapping { row: Row ->
        val mockExcel = MockExcelFile()
        mockExcel.column1 = row.getCell(0).stringCellValue
        mockExcel.column2 = row.getCell(1).stringCellValue
        mockExcel.column3 = row.getCell(2).stringCellValue
        mockExcel.column4 = row.getCell(3).stringCellValue
        mockExcel
      }
      ?.read(all = true)
      ?.result!!

    Assertions.assertEquals(expectedSize, result.size)
    Assertions.assertEquals("fila1", result[0].column1)
    Assertions.assertEquals("COD 1", result[0].column2)
    Assertions.assertEquals("fila9", result[8].column1)
    Assertions.assertEquals("COD 3.1.1", result[8].column2)
  }

  companion object {
    private const val EXCEL_MIME_TYPE = "application/vnd.ms-excel"

    private fun createExcelReaderObject(fileName: String): ExcelReader<MockExcelFile>? {
      return try {
        val resource = javaClass.getResource(String.format("/%s", fileName))
        val excelFile = File(resource!!.file)
        val excelInputStream = FileInputStream(excelFile)
        val multiPartFile = MockMultipartFile(fileName, fileName, EXCEL_MIME_TYPE, excelInputStream)
        ExcelReader(multiPartFile)
      } catch (ex: Exception) {
        null
      }
    }
  }

}
