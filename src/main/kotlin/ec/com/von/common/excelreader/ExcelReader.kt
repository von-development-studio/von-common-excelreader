/**
 * @Class:      ExcelReader
 * @Created:    07-06-2019
 * @Updated:    07-12-2022
 * @CopyRight:	VON Development Studio 2017
 */
package ec.com.von.common.excelreader

import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.util.*
import java.util.function.BiFunction
import java.util.function.Function

/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
open class ExcelReader<T>(
  private val excelFile: MultipartFile
) {
  open var result = ArrayList<T>()
  open val errorsReport = ArrayList<ExcelReaderErrorReport>()
  private val workbook: XSSFWorkbook
  private var sheet: XSSFSheet? = null
  private var headerHeight: Int? = 0
  private var endConditionFn: Function<Row, Boolean>? = null
  private var mapRowFn: Function<Row, T>? = null
  private val validations = ArrayList<ExcelReaderValidationArray<T>>()
  private var itemsRowValue = ArrayList<Row>()

  init {
    try {
      workbook = XSSFWorkbook(excelFile.inputStream)
      sheet = workbook.getSheetAt(0)
    } catch (ex: Exception) {
      throw ExcelReaderException(ex, excelFile.originalFilename!!)
    }
  }

  open fun setSheet(index: Int): ExcelReader<T> {
    sheet = workbook.getSheetAt(index)
    return this
  }

  open fun setHeader(headerHeight: Int): ExcelReader<T> {
    this.headerHeight = headerHeight
    return this
  }

  open fun setEndCondition(endConditionFn: Function<Row, Boolean>): ExcelReader<T> {
    this.endConditionFn = endConditionFn
    return this
  }

  open fun setMapping(mapRowFn: Function<Row, T>): ExcelReader<T> {
    this.mapRowFn = mapRowFn
    return this
  }

  open fun addValidation(
    conditions: Function<List<T>, List<ExcelReaderValidationCondition<T>>>
  ): ExcelReader<T> {
    validations.add(
      ExcelReaderValidationArray(
        conditions = conditions
      )
    )
    return this
  }

  @Deprecated(
    "This override is not adding new validators. Pleasem, don't use this override anymore. This will be remove on the next release",
    ReplaceWith("addValidation")
  )
  open fun addValidation(
    validation: BiFunction<T, T, Boolean>,
    mapper: BiFunction<String, T, ExcelReaderErrorReport>
  ): ExcelReader<T> {
    return this
  }

  @Deprecated(
    "This override is not adding new validators. Pleasem, don't use this override anymore. This will be remove on the next release",
    ReplaceWith("this")
  )
  open fun addValidation(
    validation: Function<T, Boolean>,
    mapper: BiFunction<String, T, ExcelReaderErrorReport>
  ): ExcelReader<T> {
    return this
  }

  @Throws(ExcelReaderException::class)
  open fun read(all: Boolean = false): ExcelReader<T> {
    validationBeforeReading()

    result = ArrayList()
    itemsRowValue = ArrayList()
    if (all) {
      val sheetsQty = workbook.numberOfSheets
      for (idx in 0 until sheetsQty) {
        val innerSheet = workbook.getSheetAt(idx)
        readSheet(innerSheet)
      }
      validateSheet()
    } else {
      readSheet(sheet!!)
      validateSheet()
    }

    try {
      workbook.close()
      return this
    } catch (ex: IOException) {
      throw ExcelReaderException(ex, excelFile.originalFilename!!)
    }
  }

  @Throws(ExcelReaderException::class)
  private fun validationBeforeReading() {
    if (endConditionFn == null) {
      throw ExcelReaderException(
        filename = excelFile.originalFilename,
        message = "End condition no defined"
      )
    }
    if (mapRowFn == null) {
      throw ExcelReaderException(
        filename = excelFile.originalFilename,
        message = "Map method no defined"
      )
    }
  }

  private fun setInformation(row: Row): String {
    return "${sheet!!.sheetName}|${row.rowNum}"
  }

  @Throws(ExcelReaderException::class)
  private fun readSheet(sheet: XSSFSheet) {
    val rowIterator = sheet.iterator()
    for (i in 0 until headerHeight!!) {
      rowIterator.next()
    }

    while (rowIterator.hasNext()) {
      val row = rowIterator.next()

      val shouldEnd = endConditionFn!!.apply(row)
      if (shouldEnd) {
        break
      }

      val rowValue = mapRowFn!!.apply(row)
      result.add(rowValue)
      itemsRowValue.add(row)
    }
  }

  @Throws(ExcelReaderException::class)
  private fun validateSheet() {
    validations.forEach { array ->
      array.conditions.apply(result).forEachIndexed { idx, it ->
        if (!it.validation) {
          if (it.mapper == null) {
            throw ExcelReaderException(
              filename = excelFile.originalFilename,
              message = "You need to set the mapper for the validation error"
            )
          }
          val information = setInformation(itemsRowValue[idx])
          val rowValue = mapRowFn!!.apply(itemsRowValue[idx])
          val error = it.mapper!!.apply(information, rowValue)
          errorsReport.add(error)
        }
      }
    }
  }

}
