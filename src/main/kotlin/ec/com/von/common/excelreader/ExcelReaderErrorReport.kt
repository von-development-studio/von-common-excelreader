/**
 * @Class:      ExcelReaderErrorReport
 * @Created:    08-06-2019
 * @Updated:    20-07-2019
 * @CopyRight:	VON Development Studio 2017
 */
package ec.com.von.common.excelreader

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.text.SimpleDateFormat

/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
data class ExcelReaderErrorReport(
  var sheet: String? = null,
  var column: String? = null,
  var row: Int? = null,
  var detail: String? = null,
  private val LOG: Logger = LoggerFactory.getLogger(ExcelReaderErrorReport::class.java)
) {
  override fun toString(): String {
    val mapper = ObjectMapper()
    mapper.dateFormat = SimpleDateFormat("dd-MM-yyyy")
    return try {
      mapper.writeValueAsString(this)
    } catch (ex: JsonProcessingException) {
      LOG.error(ex.message, ex)
      "{}"
    }
  }
}
