/**
 * @Class:      ExcelReaderValidationDatabase
 * @Created:    08-06-2019
 * @Updated:    07-12-2022
 * @CopyRight:	VON Development Studio 2017
 */
package ec.com.von.common.excelreader

import java.util.function.BiFunction
import java.util.function.Function

/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
@Deprecated("This class is not needed anymore and will be remove on the next release")
data class ExcelReaderValidationDatabase<T>(
  var validation: Function<T, Boolean>? = null,
  var mapper: BiFunction<String, T, ExcelReaderErrorReport>? = null
)
