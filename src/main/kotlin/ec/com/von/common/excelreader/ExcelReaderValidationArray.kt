/**
 * @Class:      ExcelReaderValidationArray
 * @Created:    08-06-2019
 * @Updated:    07-12-2022
 * @CopyRight:	VON Development Studio 2017
 */
package ec.com.von.common.excelreader

import java.util.function.BiFunction
import java.util.function.Function

/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
data class ExcelReaderValidationArray<T>(
  var conditions: Function<List<T>, List<ExcelReaderValidationCondition<T>>>,
  @Deprecated("")
  var validation: Function<List<T>, Boolean>? = null,
  @Deprecated("")
  var mapper: BiFunction<String, T, ExcelReaderErrorReport>? = null
)
