/**
 * @Class:      ExcelReaderValidationArray
 * @Created:    07-12-2022
 * @Updated:
 * @CopyRight:	VON Development Studio 2017
 */
package ec.com.von.common.excelreader

import java.util.function.BiFunction
import java.util.function.Function

/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
data class ExcelReaderValidationCondition<T>(
  var validation: Boolean,
  var mapper: BiFunction<String, T, ExcelReaderErrorReport>?
)
