/**
 * @Class:      ExcelReaderException
 * @Created:    07-06-2019
 * @Updated:    20-07-2019
 * @CopyRight:	VON Development Studio 2017
 */
package ec.com.von.common.excelreader

/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
data class ExcelReaderException(
  override val cause: Throwable? = null,
  override val message: String? = null,
  val filename: String? = null
) : Exception()
